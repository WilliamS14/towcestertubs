import Link from "next/link";
import TubCards from "@/Components/TubCards/TubCards";
import s from "./tubs.module.scss";
import image1 from "../../../public/Images/image1.jpg";
import image2 from "../../../public/Images/image2.jpg";
import image3 from "../../../public/Images/image3.jpg";
import image4 from "../../../public/Images/image4.jpg";
import large from "../../../public/Images/large.jpeg";
import xlarge from "../../../public/Images/xlarge.jpeg";
import coupleImage from "../../../public/Images/couple2.jpeg";
import Description from "@/Components/Description/Description";

function Home() {
  return (
    <div className={s.tubPage}>
      <div className={s.tubsDescription}>
        <Description
          title={""}
          description={
            "We have a variety of Hot Tubs available. Please send an enquiry through our contact page if you would like to hire one out."
          }
        />
        <Description
          title={""}
          description={
            "All prices will be made available upon the initial request being sent."
          }
        />
      </div>
      <div className={s.cards}>
        <Link href="/tub-couple">
          <TubCards
            tubs={TubCards}
            src={coupleImage}
            alt="Couple Image"
            title={"Couple"}
            subTitle={"For 2 people"}
            imageSrc="../../Images/couple.webp"
          />
        </Link>
        <Link href="/tub-smallfamily">
          <TubCards
            tubs={TubCards}
            src={image1}
            alt="Family 3/4 Image"
            title={"Small Family"}
            subTitle={"For 3-4 people"}
            imageSrc="../../Images/image1.jpg"
          />
        </Link>
        <Link href="/tub-largefamily">
          <TubCards
            tubs={TubCards}
            src={image3}
            alt="Family 4/6 Image"
            title={"Large Family"}
            subTitle={"For 4-6 people"}
            imageSrc="../../Images/image3.jpg"
          />
        </Link>
        <Link href="/tub-smallgroup">
          <TubCards
            tubs={TubCards}
            src={large}
            alt="Family 4/6 Image"
            title={"Small Group"}
            subTitle={"For 6-8 people"}
            imageSrc="../../Images/large.jpg"
          />
        </Link>
        <Link href="/tub-largegroup">
          <TubCards
            tubs={TubCards}
            src={xlarge}
            alt="Family 8/10 Image"
            title={"Large Group"}
            subTitle={"For 8+ people"}
            imageSrc="../../Images/xlarge.jpeg"
          />
        </Link>
        <Link href="/tub-weekenddeal">
          <TubCards
            tubs={TubCards}
            src={image2}
            alt="Weekend Deal"
            title={"Weekend Deal!"}
            subTitle={"£99 a night!"}
            imageSrc="../../Images/image2.jpg"
          />
        </Link>
      </div>
    </div>
  );
}

export default Home;
