import Description from "@/Components/Description/Description";
import s from "../detailtubstyle.module.scss";
import DetailView from "@/Components/DetailView/DetailView";
import coupleImage from "../../../public/Images/couple2.jpeg";
import Link from "next/link";

function detail() {
  return (
    <div>
      <Link href="/tubs">
        <div>
          <button className={s.returnBtn}>Back to results</button>
        </div>
      </Link>
      <div className={s.detailViewContainer}>
        <div className={s.imageContainer}>
          <DetailView src={coupleImage} alt={""} />
          <div className={s.textContainer}>
            <div className={s.textInfo}>
              <Description
                title={"The Couple's Tub"}
                description={
                  "This is a great Hot Tub for a couple or for friends of up to x2 people."
                }
              />
              <Description
                title={""}
                description={
                  "Please let us know the how many nights you would like to hire this for in your enquiry email."
                }
              />
              <Link href={"/contact"}>
                <div className={s.enquiryContainer}>
                  <button className={s.btnEnquiry}>Send enquiry</button>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default detail;
