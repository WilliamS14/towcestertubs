import Description from "@/Components/Description/Description";
import s from "../detailtubstyle.module.scss";
import DetailView from "@/Components/DetailView/DetailView";
import image2 from "../../../public/Images/image2.jpg";
import Link from "next/link";

function detail() {
  return (
    <div>
      <Link href="/tubs">
        <div>
          <button className={s.returnBtn}>Back to results</button>
        </div>
      </Link>
      <div className={s.detailViewContainer}>
        <div className={s.imageContainer}>
          <DetailView src={image2} alt={""} />
          <div className={s.textContainer}>
            <div className={s.textInfo}>
              <Description title={"The Weekend Deal!"} description={""} />
              <Description
                title={""}
                description={
                  "This tub comes with a bottle of bubbly to enjoy over the weekend."
                }
              />
              <Link href={"/contact"}>
                <div className={s.enquiryContainer}>
                  <button className={s.btnEnquiry}>Send enquiry</button>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default detail;
