"use client";
import s from "./page.module.scss";
import superwideimage from "../../public/Images/superwideimage.jpeg";
import HomepageImage from "@/Components/HomepageImage/HomepageImage";
import WelcomeSection from "@/Components/WelcomeSection/WelcomeSection";

function App() {
  return (
    <div className={s.mainPage}>
      <HomepageImage
        src={superwideimage}
        alt="wideImage"
        imageSrc="../../public/Images/superwideimage.jpeg"
      />
      <div className={s.welcomeSection}>
        <WelcomeSection />
      </div>
    </div>
  );
}

export default App;
