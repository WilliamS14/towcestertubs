"use client";

import { useEffect, useState } from "react";
import s from "./weather.module.scss";
import WeatherContainer from "../../Components/WeatherContainer/WeatherContainer";

async function Weather() {
  const [userLocation, setUserLocation] = useState({
    latitude: 0,
    longitude: 0,
  });

  // useEffect to get user location
  useEffect(() => {
    window.navigator.geolocation.getCurrentPosition(
      (pos) => {
        const newUserPos = {
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude,
        };
        setUserLocation(newUserPos);
        console.log("newUserPos", newUserPos);
      },
      (err) => {
        console.log(err);
      }
    );
  }, []);

  return (
    <div className={s.weather}>
      <WeatherContainer
        latitude={userLocation.latitude}
        longitude={userLocation.longitude}
      />
      ;
    </div>
  );
}

export default Weather;
