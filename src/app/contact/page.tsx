import s from "./contact.module.scss";
import Description from "@/Components/Description/Description";
import ContactForm from "../../Components/Contact/ContactForm";
import WeatherContainer from "@/Components/WeatherContainer/WeatherContainer";

function Contact() {
  return (
    <div className={s.pricesPage}>
      <div className={s.contactDescription}></div>
      <ContactForm />
    </div>
  );
}

export default Contact;
