import Description from "@/Components/Description/Description";
import s from "../detailtubstyle.module.scss";
import DetailView from "@/Components/DetailView/DetailView";
import xlarge from "../../../public/Images/xlarge.jpeg";
import Link from "next/link";

function detail() {
  return (
    <div>
      <Link href="/tubs">
        <div>
          <button className={s.returnBtn}>Back to results</button>
        </div>
      </Link>
      <div className={s.detailViewContainer}>
        <div className={s.imageContainer}>
          <DetailView src={xlarge} alt={""} />
          <div className={s.textContainer}>
            <div className={s.textInfo}>
              <Description
                title={"The Large Group Tub"}
                description={"Perfect for a large group."}
              />
              <Description
                title={""}
                description={
                  "Please let us know the how many nights you would like to hire this for in your enquiry email."
                }
              />
              <Link href={"/contact"}>
                <div className={s.enquiryContainer}>
                  <button className={s.btnEnquiry}>Send enquiry</button>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default detail;
