"use client";
import Link from "next/link";
import s from "./HomepageImage.module.scss";
import Image from "next/image";

interface HomepageImageProps {
  imageSrc: string;
  src: any;
  alt: string;
}

const HomepageImage: React.FC<HomepageImageProps> = ({ src }) => {
  return (
    <div className={s.imageContainer}>
      <Image
        src={src}
        alt={s.wideimagelarge}
        className={s.homepageImageContainer}
      />
      <div className={s.textOverlay}>
        <h1 className={s.titleText}>
          Hot Tub hire in Towcester
          <br />
          and the surrounding areas
        </h1>
      </div>
      <Link className={s.btnClickthrough} href="/tubs">
        <button className={s.btnClickthroughToTubs}>Find your tub</button>
      </Link>
    </div>
  );
};

export default HomepageImage;
