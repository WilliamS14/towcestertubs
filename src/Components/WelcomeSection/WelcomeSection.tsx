import React from "react";
import s from "./WelcomeSection.module.scss";

const WelcomeSection = () => {
  return (
    <section className={s.mainContainer}>
      <div className={s.textContainer}>
        <h1 className={s.welcomeTitle}>Welcome to Our Website!</h1>
        <p className={s.pText}>
          At Towcester Tubs, we are passionate about delivering a fun and
          relaxing hot tub experience for our customers.
        </p>
        <p>
          When you choose to hire a hot tub with us, you don’t just get a
          fantastic experience to enjoy with family and friends – you get a
          personalised service and a commitment to quality and safety.
        </p>
        <p className={s.pText}>
          We offer a luxurious way to enjoy time with your loved ones – and
          what’s more we have hot tub rental options to suit every budget and
          event.
        </p>
        <p>
          Planning a special occasion or looking to create special memories?
          Look no further. We are the local hot tub hire specialists – serving
          Northamptonshire, Bedfordshire, Buckinghamshire, Essex, Suffolk,
          Cambridgeshire and Derbyshire.
        </p>
        <p>
          Celebrate that special anniversary with a jacuzzi tub hire. Throwing a
          party or having guests over? Our inflatable hot tub hire service is
          perfect for birthdays, house parties or as a treat to add some glamour
          and excitement to time spent at home.
        </p>
        <p>
          All prices are inclusive of delivery, collection and installation. All
          you need is a stable outdoor area and access to a tap – and you are
          good to go!
        </p>
        <p>
          Our service is all about simplicity and service. If you have any
          questions, please get in touch and we will be happy to help.
        </p>
      </div>
    </section>
  );
};

export default WelcomeSection;
