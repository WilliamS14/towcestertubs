import Image from "next/image";
import s from "./DetailView.module.scss";
import Description from "@/Components/Description/Description";

interface detailViewProps {
  src: any;
  alt: string;
}

const detailView: React.FC<detailViewProps> = ({ src, alt }) => {
  return (
    <div className={s.imageContainer}>
      <div className={s.image}>
        <Image src={src} alt={s.imgTitle} className={s.imageSize} />
      </div>
    </div>
  );
};

export default detailView;
