"use client";

import React, { useState, useRef } from "react";
import { sendContactForm } from "../../services";
import s from "./ContactForm.module.scss";

const ContactForm = () => {
  const [message, setMessage] = useState("");
  const formRef = useRef();

  const submitContact = async (e: any) => {
    e.preventDefault();
    console.log(e);
    const res = await sendContactForm({
      firstName: e.target[0].value,
      lastName: e.target[1].value,
      email: e.target[2].value,
      contactNumber: e.target[3].value,
      comment: e.target[4].value,
    });
    if (res == 0) {
      setMessage("Thank you for your valuable comment!");
      formRef.current.reset();
    } else {
      setMessage("Something went wrong! Please try again");
    }
  };

  return (
    <div className={s.contactFormContainer}>
      <h1 className={s.contactTitle}>{"Contact Us"}</h1>
      <div>
        <div>
          {message}
          <span onClick={() => setMessage("")}></span>
        </div>
        <form ref={formRef} onSubmit={submitContact}>
          <input
            required
            placeholder="First Name*"
            type={"text"}
            minLength={3}
            maxLength={25}
            className={s.inputField}
          />
          <input
            required
            placeholder="Last Name*"
            type={"text"}
            minLength={3}
            maxLength={25}
            className={s.inputField}
          />
          <input
            required
            placeholder="Email Address*"
            type={"email"}
            className={s.inputField}
          />
          <input
            required
            placeholder="Contact Number*"
            type={"text"}
            className={s.inputField}
          />
          <textarea
            required
            placeholder="Comment*"
            rows={5}
            className={s.inputField}
          ></textarea>
          <button type="submit" className={s.contactBtn}>
            Send
          </button>
        </form>
      </div>
    </div>
  );
};

export default ContactForm;
