import React from "react";
import s from "./Footer.module.scss";

const Footer = () => {
  return (
    <div className={s.mainContainer}>
      <div className={s.container}>
        <div className={s.leftSide}>
          <h3>Follow us</h3>
        </div>
      </div>
      <p>
        &copy; {new Date().getFullYear()} Your Website. All rights reserved.
      </p>
    </div>
  );
};

export default Footer;
