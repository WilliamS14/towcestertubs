import Image from "next/image";
import s from "./TubCards.module.scss";

interface TubCardProps {
  title: string;
  subTitle: string;
  imageSrc: string;
  tubs: any;
  src: any;
  alt: string;
}

const TubCards: React.FC<TubCardProps> = ({
  title,
  subTitle,
  tubs,
  src,
  alt,
}) => {
  return (
    <div className={s.cards}>
      <div className={s.cardContainer}>
        <div className={s.cardImage}>
          <Image src={src} alt={s.imgTitle} className={s.cardImageSize} />
        </div>
        <div className={s.cardPrice}>
          <div className={s.cardTitle}>{title}</div>
          <div className={s.cardSubTitle}>{subTitle}</div>
        </div>
      </div>
    </div>
  );
};

export default TubCards;
