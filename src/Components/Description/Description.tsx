import s from './Description.module.scss';

interface DescriptionProps {
  title: string;
  description: string;
}

const Description: React.FC<DescriptionProps> = ({ title, description }) => {
  return (
    <div className={s.description}>
      <h2 className={s.descriptionTitle}>{title}</h2>
      <p className={s.descriptionText}>{description}</p>
    </div>
  );
};

export default Description;
