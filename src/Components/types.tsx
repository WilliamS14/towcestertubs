export interface CardInterface {
    indicator?: string,
    image?: string,
    title?: string,
    subtitle?: string,
    body?: string,
}