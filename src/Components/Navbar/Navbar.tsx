import Link from "next/link";
import s from "./Navbar.module.scss";
import Title from "../Title/Title";
import logosmall from "../../../public/Images/logosmall.jpg";
import Image from "next/image";

// interface HeaderProps {
//     title: string;
//     navItems: string[];
//   }

function Navbar() {
  return (
    <nav className={s.navbar}>
      <div className={s.navbarLogo}>
        <Title text={"Towcester Tubs "} />
        {/* <Image src={logosmall} alt={s.logo} className={s.logo} /> */}
      </div>
      <div className={s.navListItems}>
        <ul className={s.navbarNav}>
          <li className={s.navItem}>
            <Link href="/" className={s.navLink}>
              Home
            </Link>
          </li>
          <li className={s.navItem}>
            <Link href="/tubs" className={s.navLink}>
              Tubs
            </Link>
          </li>
          <li className={s.navItem}>
            <Link href="/contact" className={s.navLink}>
              Contact
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navbar;
