"use client";

import s from "./WeatherContainer.module.scss";
import { useEffect, useState } from "react";

type WeatherContainerProps = {
  latitude: number;
  longitude: number;
};

type WeatherDataResponse = {
  latitude: number;
  longitude: number;
  generationtime_ms: number;
  utc_offset_seconds: number;
  timezone: string;
  timezone_abbreviation: string;
  elevation: number;
  hourly_units: {
    time: string;
    temperature_2m: string;
    relativehumidity_2m: string;
    precipitation_probability: string;
    cloudcover: string;
    windspeed_10m: string;
    winddirection_10m: string;
  };
  hourly: {
    time: string[];
    temperature_2m: number[];
    relativehumidity_2m: number[];
    precipitation_probability: number[];
    cloudcover: number[];
    windspeed_10m: number[];
    winddirection_10m: number[];
  };
};

async function getWeatherData(latitude: number, longitude: number) {
  const res = await fetch(
    `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&hourly=temperature_2m,relativehumidity_2m,precipitation_probability,cloudcover,windspeed_10m,winddirection_10m`
  );
  const data = res.json();
  return data;
}

const WeatherContainer = ({ latitude, longitude }: WeatherContainerProps) => {
  const [weatherData, setWeatherData] = useState<WeatherDataResponse>();

  useEffect(() => {
    async function fetchData() {
      const data = await getWeatherData(latitude, longitude);
      setWeatherData(data);
    }
    fetchData();
  }, [latitude, longitude]);

  return (
    <>
      {weatherData && (
        <div className={s.weatherCard}>
          <div className={s.mainTitle}>Local Weather</div>
          <div className={s.infoContainer}>
            <div className={s.titleContainer}>
              <div className={s.title}>Temperature:</div>
            </div>
            <div className={s.resultContainer}>
              <div className={s.result}>
                {weatherData.hourly.temperature_2m[0]}
                {weatherData.hourly_units.temperature_2m}
              </div>
            </div>
          </div>

          <div className={s.infoContainer}>
            <div className={s.titleContainer}>
              <div className={s.title}>Humidity:</div>
            </div>
            <div className={s.resultContainer}>
              <div className={s.result}>
                {weatherData.hourly.relativehumidity_2m[0]}
                {weatherData.hourly_units.relativehumidity_2m}
              </div>
            </div>
          </div>

          <div className={s.infoContainer}>
            <div className={s.titleContainer}>
              <div className={s.title}>Rain Probability:</div>
            </div>
            <div className={s.resultContainer}>
              <div className={s.result}>
                {weatherData.hourly.precipitation_probability[0]}
                {weatherData.hourly_units.precipitation_probability}
              </div>
            </div>
          </div>

          <div className={s.infoContainer}>
            <div className={s.titleContainer}>
              <div className={s.title}>Cloud Cover:</div>
            </div>
            <div className={s.resultContainer}>
              <div className={s.result}>
                {weatherData.hourly.cloudcover[0]}
                {weatherData.hourly_units.cloudcover}
              </div>
            </div>
          </div>

          <div className={s.infoContainer}>
            <div className={s.titleContainer}>
              <div className={s.title}>Wind Speed:</div>
            </div>
            <div className={s.resultContainer}>
              <div className={s.result}>
                {weatherData.hourly.windspeed_10m[0]}
                {weatherData.hourly_units.windspeed_10m}
              </div>
            </div>
          </div>

          <div className={s.infoContainer}>
            <div className={s.titleContainer}>
              <div className={s.title}>Wind Direction:</div>
            </div>
            <div className={s.resultContainer}>
              <div className={s.result}>
                {weatherData.hourly.winddirection_10m[0]}
                {weatherData.hourly_units.winddirection_10m}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default WeatherContainer;
