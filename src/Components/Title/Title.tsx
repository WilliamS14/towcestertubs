import s from './Title.module.scss';

interface TitleProps {
    text: string;
    level?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
    titleText?: string;
  }

const Title: React.FC<TitleProps> = ({ text, level = "h1", titleText }) => {
  const HeadingTag = level;
  return <HeadingTag className={s.titleText}>{text}</HeadingTag>;
};

export default Title;