// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "firebase/firestore/lite";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAemSAYb-J--dD0GXcog5rJwdzQwutckHg",
  authDomain: "towcestertubs-731d8.firebaseapp.com",
  projectId: "towcestertubs-731d8",
  storageBucket: "towcestertubs-731d8.appspot.com",
  messagingSenderId: "539652710139",
  appId: "1:539652710139:web:03412be10b8992909e9a5e",
  measurementId: "G-GDNSQJ63E3"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const firestore = getFirestore(app);