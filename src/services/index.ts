import { addDoc, collection, Timestamp } from "firebase/firestore/lite";
import { firestore } from "../firebaseConfig";

type ContactFormProps = {
  firstName: string;
  lastName: string;
  contactNumber: number;
  email: string;
  comment: string;
}

export const sendContactForm = async ({ firstName, lastName, email, contactNumber, comment }: ContactFormProps) => {
  try {
    const ref = collection(firestore, "contact");
    await addDoc(ref, {
      firstName,
      lastName,
      email,
      contactNumber,
      comment,
      sentAt: Timestamp.now().toDate(),
    });
    return 0;
  } catch (err) {
    console.log(err)
    return -1;
  }
};